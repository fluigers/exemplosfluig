//CHAMADA DE WEB SERVICE(SOAP)  DENTRO DE UMA WIDGET

WCMAPI.Create({
    url: "/webdesk/ECMCardService?wsdl", //URL do Web Service
    contentType: "text/xml", //Tipo do dado a ser ENVIADO ao servidor
    dataType: "xml", //Tipo dos dados que você espera RECEBER do servidor	
    data: SEU_XML, // Dados que serão enviados ao servidor
    success: function(response){
		//Código realizado quando a chamada foi um sucesso. O parametro 'response' é o dado retornado da requisição
	},
    error: function (msg){
        //Código realizado quando a chamada foi uma falha. O parametro 'msg' é a mensagem do erro em questão
    }
});

//EXISTE ALGUMAS FORMAS DE CONSTRUIR O SEU_XML:

//>>>>>>>>>>>>>>>>>>>>>>>>>> VIA AJAX: <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	$.ajax({
		async: false,
		dataType: "xml",
		success: function(xml) {
			_xml = $(xml);
		},
		type: "GET",
		url: "/NOME_WIDGET/resources/js/xmls/ECMCardService_create.xml" //Caminho do XML dentro de sua widget
	});
	    	
	//PREENCHER CAMPOS DO XML, EXEMPLOS:	
	_xml.find("companyId").text( WCMAPI.tenantCode ); //Preenche procurando a TAG	
	_xml.find("[name=nome]").text("Meu teste"); //Preenche procurando por NAME	
	_xml.find("[id=senha").text(153);//Preenche procurando por ID

	// EXEMPLO DE CHAMADA COM O XML BUSCADO E JÁ PREENCHIDO:
   	WCMAPI.Create({
	    url: "/webdesk/ECMCardService?wsdl",
	    contentType: "text/xml",
	    dataType: "xml",
	    data: _xml[0], //O XML A SER ENVIADO
	    success: function(data){
	    	//Código realizado quando a chamada foi um sucesso. O parametro 'data' é o dado retornado da requisição
	    }
    });

//>>>>>>>>>>>>>>>>>>>>>>>>>> VIA CONSTRUÇÃO EM CÓDIGO: <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

   		var parser = new DOMParser(); //Criação do parser 
   		//Seu envelope SOAP (O XML a ser enviado) em String:		   
    	var xmlSoapEmString = "<soapenv:Envelope xmlns:soapenv=\"http:\/\/schemas.xmlsoap.org\/soap\/envelope\/\" xmlns:ws=\"http:\/\/ws.dm.ecm.technology.totvs.com\/\"> <soapenv:Header\/> <soapenv:Body> <ws:create> <companyId><\/companyId> <username><\/username> <password><\/password> <card id=\"card\"> <\/card> <\/ws:create> <\/soapenv:Body> <\/soapenv:Envelope>";
    	var xmlSoapRequest= parser.parseFromString(xmlSoapEmString,"text/xml"); //Transformar a String em XML
    		
    	//PREENCHER CAMPOS DO XML, EXEMPLOS:
    	xmlSoapRequest.getElementsByTagName('companyId')[0].innerHTML = WCMAPI.tenantCode;    		
    	xmlSoapRequest.getElementsByTagName('username')[0].innerHTML = "username";
    	xmlSoapRequest.getElementsByTagName('password')[0].innerHTML = "12345";

		// EXEMPLO DE CHAMADA COM O XML CRIADO  E JÁ PREENCHIDO:
		WCMAPI.Create({
		    url: "/webdesk/ECMCardService?wsdl",
		    contentType: "text/xml",
		    dataType: "xml",
		    data: xmlSoapRequest, //O XML A SER ENVIADO
		    success: function(data){
		    	//Código realizado quando a chamada foi um sucesso. O parametro 'data' é o dado retornado da requisição	    		    	
		    }
		 });