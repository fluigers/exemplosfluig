//CHAMADA API VIA AJAX JQUERY - GET

$.ajax({
  	async : false, //Determina que a chamada seja feita de forma assíncrona (true) ou síncrona (false).
	type : "GET", //Tipo da requisição
	dataType : "json", //Tipo dos dados que você espera RECEBER do servidor
	url : '/api/public/social/listUsersWithRelevance', //URL da API
	success: function(response){
		//Código realizado quando a chamada foi um sucesso. O parametro 'response' é o dado retornado da requisição
	},
    error: function (msg){
        //Código realizado quando a chamada foi uma falha. O parametro 'msg' é a mensagem do erro em questão
    }
});


//CHAMADA API VIA AJAX JQUERY - POST

$.ajax({
  	async : false, //Determina que a chamada seja feita de forma assíncrona (true) ou síncrona (false).
	type : "POST", //Tipo da requisição 
	contentType: "application/json", //Tipo do dado a ser ENVIADO ao servidor	
	dataType : "json", //Tipo dos dados que você espera RECEBER do servidor	
	url : '/api/public/ecm/document/remove', //URL da API
    data : '{"id":"100"}', // Dados que serão enviados ao servidor
	success: function(response){
		//Código realizado quando a chamada foi um sucesso. O parametro 'response' é o dado retornado da requisição
	},
    error: function (msg){
        //Código realizado quando a chamada foi uma falha. O parametro 'msg' é a mensagem do erro em questão
    }
});
    