$(document).ready(function($) { 
	focusWorkflowActions();
 });

function focusWorkflowActions(){
	var bodyParent = parent.frames.document.body;
	var workflowActions = parent.frames.document.getElementById('workflowActions');
	var divOverlay = document.createElement('div');	
	
	$(divOverlay).css({
 		'background':'rgba(0,0,0,0.45)',
		'display':'none',
		'width':'100%', 
		'height':'100%',
		'position':'absolute', 
		'top':'0', 'left':'0',
		'z-index':'99998'
	});
	
	$(workflowActions).css({
 		'z-index':'99999',
 		'border-style': 'solid',
 		'border-color': '#fff',
 		'border-width': '15px'
 	});
	
	$(bodyParent).append(divOverlay); 
	
    $(divOverlay).fadeIn(300);   	
 	
 	$(bodyParent).click(function(){ 		
 		$(divOverlay).remove();
 	});
}