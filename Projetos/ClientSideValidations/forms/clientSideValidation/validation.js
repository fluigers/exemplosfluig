$(document).ready(function() {
	validation();
});

function validation(){
	$('#myForm').validator({
		'feedback': {
		  'success': 'fluigicon-verified',
		  'error': 'fluigicon-remove'
		}	
	});
}

function validateWhenSubmit(){
	$('#myForm').validator('validate');
	var errorsCount = $(".has-danger").length;
	return  errorsCount == 0;
}

var beforeMovementOptions = function(numState){
	return validateWhenSubmit();
}

var beforeSendValidate = function(numState,nextState){
	return validateWhenSubmit();
}