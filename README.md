# Exemplos Fluig
Repositório dedicado a reunir exemplos e trechos de códigos que a equipe acha relevante para o uso em seu desenvolvimento.
## Pastas e Arquivos
 Abaixo está listado todos os exemplos do repositório com um breve resumo de sua funcionalidade/uso;
 
### Formulários:
> Pasta dedicada a todos os exemplos referente ao desenvolvimento de forumários.

>#### Estruturas HTMLs 
 1. **exemploPanels.html**: Exemplo da estrutura do uso de painels e panels group (com collapse);
 1. **exemploTabs.html**: Exemplo da estrutura do uso de tabs;

>#### Events 
 1. **exemplosDisplayFields.js**: Exemplo do uso do evento displayFields com diversas funções genéricas;

>#### JavaScripts
 1. **AJAX.js**: Exemplo da estrutura do AJAX, fazendo uma consulta a API do Fluig em ambos os verbos do HTTP, GET e POST;
 1. **pegarTodosInputsDiv.js**: Código para gerar um array com todos os campos de uma div (usado nas funções do displayFields e ValidadeForm);
 1. **reloadZoom.js**: Exemplo do uso da função *reloadZoomFilterValues* e consultar campos extras do elemento selecionado no zoom em um dataset (forma de trazer dados sem mostrar do zoom);


### WebServices:
> Pasta dedicada a todos os exemplos referente ao desenvolvimento de WebServices (SOAP E REST).

>#### SOAP 
 1. **chamadaEmEventos.html**: Pequeno trecho de uma chamada ao um Webservice em um evento;
 1. **chamadaEmWidget.html**: Chamada de webservices dentro de uma Widget, exemplo contém duas formas do uso de um XML. Com Ajax e com construção dinâmica no JS.
>#### REST 
 1. **UsandoOAuth**: Projeto de exemplo de consulta a API que consome um dataset (colleague) usando OAuth para uso em páginas públicas;

### Workflows:
> Pasta dedicada a todos os exemplos referente ao desenvolvimento de workflows.

>#### JavaScript 
 1. focusWorkFlowActions.js: Script que tem como finalidade 'focar' (de maneira visual) os actions do workflow (botão Enviar, Salvar e Descartar);

### Projetos:
> Pasta dedicada a todos os projetos de exemplos, que unem diversos arquivos em seu domínio.

>#### ClientSideValidations
* Projeto contendo um exemplo do uso de validações no client side (browser) usando uma biblioteca externa ([Validator](http://1000hz.github.io/bootstrap-validator/)) e métodos do Fluig para execução de tal validação antes do envio de um atividade.