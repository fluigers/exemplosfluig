//Códigos genéricos para DisplayFields:

function displayFields(form,customHTML){ 

	//Mostrar no view os inputs no interior de caixas desabilitadas
	form.setShowDisabledFields(true);

	var nrAtividade = getValue("WKNumState");
	
	customHTML.append("<script>");
	customHTML.append("$(document).ready(function(){ ");
	
	var NrDaSuaAtividade = 10;
	
	//Array com todos os NAMES dos campos de uma div/panel/tab - Ver 'pegarTodosInputsDiv.js' para facilitar na criação dessa variavel;
	var campos_DIV_X = ["campo1","campo2","campo3"];

	
	if (nrAtividade == NrDaSuaAtividade) {
		//Desabilitar todos os campos de uma div:
		desabilitarCampos(campos_DIV_X);

		//Habilitar todos os campos de uma div:
		habilitarCampos(campos_DIV_X);
	
		//Esconder um campo:
		esconderCampo('campo1');

		//Mostrar um campo:
		mostrarCampo('campo1');

		//Desabilitar apenas um campo:
		form.setEnabled('campo1', false);

		//Habilitar apenas um campo:
		form.setEnabled('campo1', true);

		//Definir uma Tab que deve ser aberta quando o formulário abrir. Ver 'exemploTabs.html'		
		definirTabInicial("#idDoGrupoDaTab", "#idDaTab2");

		//Pegar valor do formulário:
		var campo1 = form.getValue('campo1');

		//Setar valor no formulário:
		form.setValue('campo1', "O QUE DESEJO INSERIR");

		//Inserir nome do usuário logado:
		var nomeSolicitante = getNomeSolicitante(getValue('WKUser'));
		form.setValue('campoNomeSolicitante', nomeSolicitante);

	}


	 customHTML.append("});");
	 customHTML.append("</script>");
	 

	//FUNÇÕES GENÉRICAS:

	function habilitarCampos(campos){
		campos.forEach(function(campo){ 
		 form.setEnabled(campo, true); 
		});		
	}
		
	function desabilitarCampos(campos){
		campos.forEach(function(campo){ 
		 form.setEnabled(campo, false); 
		});		
	}

	function esconderCampo(seletorCampo){
		customHTML.append("$('"+seletorCampo+"').hide();");
	}

	function mostrarCampo(seletorCampo){
		customHTML.append("$('"+seletorCampo+"').show();");
	}

	function definirTabInicial(grupoTabs, idTab){
		customHTML.append('$("'+grupoTabs+' a[href='+"'"+idTab+"']"+'").tab("show");');
	}

	function getNomeSolicitante(matUsuario){
		var c1 = DatasetFactory.createConstraint("colleaguePK.colleagueId", matUsuario, matUsuario, ConstraintType.MUST);
		var filtro = new Array(c1);
		var dataset = DatasetFactory.getDataset("colleague", null, filtro, null);
		return dataset.getValue(0,"colleagueName");
	}

}