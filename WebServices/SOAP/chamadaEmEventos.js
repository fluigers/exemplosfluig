//CHAMADA DE WEB SERVICE(SOAP) DENTRO DE UM EVENTO:

//1 - INSERIR SERVIÇO NO SERVIDOR (VIA ECLIPSE)
//2 - CONSULTAR SUAS CLASSES E MÉTODOS

//EX. CÓDIGO:

	var ServiceProvider = ServiceManager.getServiceInstance("WS_PROTHEUS_TOTVSRS");	//Retorna a instancia do serviço (parâmetro: código do serviço)
	var serviceLocator = ServiceProvider.instantiate("teste.TRSPC020"); //Retorna a instancia da classe (parâmetro: nome da classe com pacote)
	var service = serviceLocator.getTRSPC020SOAP(); // Executa um método que retorna uma intancia de outra classe (objeto)
	service.wsaprovapc("totvs","totvs","99","01", "372096", "obs", "L"); // Execulta o método desejado no objeto retornado acima

