//ZOOM DE COLLEAGUES MOSTRANDO EMAIL
<input type="text" name="idColleague" id="idColleague"/>
<input type="text" name="email" id="email"/>
<input type="zoom" class="form-control" name="colleagueZoom" id="colleagueZoom"
	data-zoom="{
		        'displayKey':'colleagueName',
		        'datasetId':'colleague',
		        'fields':[
		             {
		               'field':'colleagueName',
		               'label':'usuários',
		               'standard':'true',
		               'search':'true'
		            },
		            {
		               'field':'colleagueId',
		               'label':'ID'
		             },
		            {
		               'field':'mail',
		               'label':'Email'
		             }
        		]
     			}"/>




//Método chamado quando seleciona alguem no zoom
function setSelectedZoomItem(selectedItem) {
	if (selectedItem.inputName == "colleagueZoom") {
		document.getElementById("idColleague").value = selectedItem.colleagueId;
		document.getElementById("email").value = selectedItem.mail;

		// Método que vai filtrar o segundo zoom com o campo 'colleagueId' pelo id do colleague selecionado
		reloadZoomFilterValues("nomeSegundoZoom", "colleagueId," + selectedItem.idColleague);
	}
}

//PARA NÃO EXIBIR TODOS OS CAMPOS NO ZOOM (EMAIL)
<input type="text" name="idColleague" id="idColleague"/>
<input type="text" name="email" id="email"/>
<input type="zoom" class="form-control" name="colleagueZoom" id="colleagueZoom"
	data-zoom="{
		        'displayKey':'colleagueName',
		        'datasetId':'colleague',
		        'fields':[
		             {
		               'field':'colleagueName',
		               'label':'usuários',
		               'standard':'true',
		               'search':'true'
		            },
		            {
		               'field':'colleagueId',
		               'label':'ID'
		             },
        		]
     			}"/>



function setSelectedZoomItem(selectedItem) {
	if (selectedItem.inputName == "colleagueZoom") {

		var c1 = DatasetFactory.createConstraint("colleagueId", selectedItem.colleagueId, selectedItem.colleagueId, ConstraintType.MUST);
	    var dataset = DatasetFactory.getDataset("colleague", null, new Array(c1), null);

		document.getElementById("idColleague").value = selectedItem.colleagueId;

		//PREENCHER EMAIL COM O DATASET, POIS NÃO FOI INFORMADO PARA MOSTRAR NO ZOOM
		document.getElementById("email").value =  dataset.values[0].mail

		reloadZoomFilterValues("nomeSegundoZoom", "colleagueId," + selectedItem.idColleague);
	}
}